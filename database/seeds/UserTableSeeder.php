<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Mail;
use App\Mail\Invite;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = Role::all();   // Get all roles
        $user_controller = new UserController;
        $users = $user_controller->generateNames(Role::count()*2, "Netherlands");   // Get names for each users
        $counter = 0;   // Counter used for unique email and getting the first/second name.

        // We create 2 accounts for each role, one registered and one invited.
        for($i=0; $i<2; $i++)
        {
            foreach($roles as $role)
            {
                // Create new user
                $user = new User();
                $user->role_id = $role->id;
                $user->email = "futsal" . (string)($counter+1) . "@mailinator.com";  // These can be read at https://www.mailinator.com.

                if($i == 0)
                {
                    // If we're in the first sequence; we create a register token for the invite and send an email.
                    $user->register_token = "token" . (string)($counter+1);
                    Mail::to($user->email)->send(new Invite(route('register', ['token' => $user->register_token])));
                }

                // If we're in the repeat; fill fields required for a fully registered user.
                if($i == 1)
                {
                    $user->first_name = $users[$counter]["name"];
                    $user->sur_name = $users[$counter]["surname"];
                    $user->password = Hash::make("Password123");   // Hash password
                    $user->last_seen = new \DateTime(); // set initial last_seen
                }

                $user->timestamps = false;
                $user->save();
                $counter++;
            }
        }
    }
}
