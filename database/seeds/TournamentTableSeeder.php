<?php

use Illuminate\Database\Seeder;
use App\Models\Tournament;
use App\Http\Controllers\UserController;

class TournamentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $amount = 15;
        $user_controller = new UserController;
        $names = $user_controller->generateNames($amount);
        $date = new DateTime('now');
        $date->sub(new DateInterval('P' . $amount*5 . 'D'));
        
        // IN THIS FOR LOOP WE USE TO RANDOMISE IF THINGS ARE FILLED IN OR NOT
        for($i=0; $i<$amount; $i++)
        {
            // Create team
            $tournament = new Tournament();
            $tournament->name = $names[$i]["surname"] . "-Toernooi";
            $tournament->timestamps = false;

            // add between 3-20 days to the new start date. and set it.
            $days = rand(3,20);
            $date->add(new DateInterval('P' . $days . 'D'));
            $tournament->start_date = $date;    // set start date
            
            if(rand(0,2) !== 0)
            {
                // Add between x days for the end of the tournament
                $temp_date = clone $date;
                $end_days = rand(4,6);
                $temp_date->add(new DateInterval('P' . $end_days . 'D'));
                $tournament->end_date = $temp_date;
                if(rand(0,1)) $date->add(new DateInterval('P' . rand(1,4) . 'D'));
            }

            // Set a man of the tournament
            if(rand(0,1))   $tournament->mott_id = $user_controller->getRandomUserId();

            $tournament->save();
        }
    }
}
