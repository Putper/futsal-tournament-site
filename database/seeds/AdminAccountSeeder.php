<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;

class AdminAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create new user
        $user = new User();
        $user->role_id = 5; // TODO: dynamically get the admin role_id instead
        $user->email = env('ADMIN_EMAIL', 'futsal@jaspervanveenhuizen.nl');
        $user->first_name = "Admin";
        $user->sur_name = "van Site";
        // $user->password = Hash::make(str_shuffle(bin2hex(openssl_random_pseudo_bytes(4))));   // Generate a random password
        $user->password = Hash::make(env('ADMIN_PASSWORD', 'Password123'));
        $user->last_seen = new \DateTime(); // set initial last_seen

        $user->timestamps = false;
        $user->save();
    }
}
