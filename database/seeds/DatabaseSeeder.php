<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Seeders to always run
        $this->call([
            RoleTableSeeder::class,
            AdminAccountSeeder::class,
            // EmptyTeamSeeder::class
        ]);

        // Production seeder
        if( App::Environment() === 'production' )
        {
            // $this->call(
            // );
        }

        // Local seeders
        if( App::Environment() === 'local' )
        {
            $this->call([
                UserTableSeeder::class,
                RandomTeamTableSeeder::class,
                // TournamentTableSeeder::class,
                // PoolTableSeeder::class,
            ]);
        }
    }
}
