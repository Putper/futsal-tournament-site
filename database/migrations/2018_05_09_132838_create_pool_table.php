<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoolTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pool', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('number');  // number of the pool
            $table->boolean('finished');    // if the pool has been finished or not
            $table->integer('tournament_id')  ->unsigned();
            $table->foreign('tournament_id')  ->references('id')->on('tournament')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pool');
    }
}
