<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('match', function (Blueprint $table)
        {
            $table->increments('id');
            $table->boolean('has_ended');   // if the match has been ended
            $table->dateTime('start')       ->nullable();  // When the match starts
            $table->integer('field')        ->nullable();   // On which field the match is
            $table->integer('round')        ->nullable();   // Which round the match plays
            // // The tournament this match belongs to
            // $table->integer('tournament_id')    ->unsigned();
            // $table->foreign('tournament_id')    ->references('id')->on('tournament');
            // Pool this match belongs to
            $table->integer('pool_id')    ->nullable()->unsigned();
            $table->foreign('pool_id')    ->references('id')->on('pool')
                ->onDelete('cascade');
            // Results
            $table->integer('result1_id')    ->unsigned();
            $table->foreign('result1_id')    ->references('id')->on('result');
            $table->integer('result2_id')    ->unsigned();
            $table->foreign('result2_id')    ->references('id')->on('result');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('match');
    }
}
