<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('result', function (Blueprint $table)
        {
            $table->increments('id');
            // Score
            $table->integer('score')    ->nullable();
            // Teams playing
            $table->integer('team_id')  ->unsigned();
            $table->foreign('team_id')  ->references('id')->on('team');
            // Man of the matches
            $table->integer('motm')     ->nullable()->unsigned();
            $table->foreign('motm')     ->references('id')->on('user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('result');
    }
}
