{{-- @author Bjorn Zwikker <bjornzwikker@gmail.com> --}}

@extends('layouts/base')

@section('site-title', 'Scoreboard')



@section('content')
    {{-- <table id="scoreboard" class="responsive-table"> --}}
    <table id="scoreboard">
        <thead>
            <tr>
                <td></td>
                <th>Team</th>
                <th>PNT</th>
                <th>WIN</th>
                <th>VERL</th>
                <th>GLK</th>
                <th>GV</th>
                <th>GT</th>
                <th>GS</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($teams as $key => $team)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td style="white-space: nowrap">{{ $team->name }}</td>
                    <td>{{ $team->points }}</td>
                    <td>{{ $team->won }}</td>
                    <td>{{ $team->lost }}</td>
                    <td>{{ $team->tied }}</td>
                    <td>{{ $team->goals }}</td>
                    <td>{{ $team->countergoals }}</td>
                    <td>{{ $team->saldo}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{-- <div class="card-content">
        <div class="row center equal-height">
            <div class="col s12">
                <table>
                    <tbody>
                        <td>
                            <ul>
                                <li><b>Team:</b></li>
                                <li><b>PNT:</b></li>
                                <li><b>WIN:</b></li>
                                <li><b>VERL:</b></li>
                                <li><b>GLK:</b></li>
                                <li><b>GV:</b></li>
                                <li><b>GT:</b></li>
                                <li><b>GS:</b></li>
                            </ul>
                        </td>
                        <td>
                            <li>Team naam</li>
                            <li>Punten</li>
                            <li>Gewonnen wedstrijden</li>
                            <li>Verloren wedstrijden</li>
                            <li>Gelijk gespeelde wedstrijden</li>
                            <li>Goals voor</li>
                            <li>Goals tegen</li>
                            <li>Goal saldo</li>
                        </td>
                    </tbody>
                </table>
            </div>
        </div>
    </div> --}}
@endsection
