let placeholder_image = window.location.protocol + "//" + window.location.host + '/images/image-missing.png'

/**
 * Remove list where the given close butto nis in
 * @param {HTMLElement} close
 */
function removeList(close)
{
    close.parentNode.parentNode.remove();
}


/**
 * Select team for given input.
 * @param {string} input 
 * @param {string} name 
 * @param {string} result_output 
 * @param {integer} team_id 
 * @param {string} result_image 
 * @param {string} image 
 */
function selectTeam(input, name=null, result_output=null, team_id=null, result_image=null, image=null, list=null)
{
    let input_element = document.getElementById(input);
    let output = document.getElementById(result_output);
    if(list)    list.innerHTML = "";

    // Empty input
    input_element.value = "";

    // IF A LIST IS GIVEN, ASSUME WE GOTTA ADD THE RESULT TO THE LIST
    if(output.nodeName == "UL")
    {
        // Create containers
        let player_li = document.createElement('li');
        let column1 = document.createElement('div');
        let column2 = document.createElement('div');
        // Add classes
        player_li.classList.add('collection-item', 'row'); 
        column1.classList.add('col', 's12', 'm2');
        column2.classList.add('col', 's12', 'm10');

        // Create input for name
        let name_input = document.createElement('input');
        name_input.type = 'checkbox';
        name_input.name = 'teams[]';
        name_input.value = team_id;
        name_input.readOnly = true;
        name_input.checked = true;

        let team_name = document.createTextNode(name);   // create textnode with name

        let remove_icon = document.createElement('i');
        remove_icon.classList.add('mdi', 'mdi-close-circle', 'close', 'clickable');
        remove_icon.addEventListener("mousedown", removeList.bind(null, remove_icon), false);

        let logo = document.createElement('img');
        logo.classList.add('px52');   // Add class to image
        logo.src = (image) ? image : placeholder_image;   // set image source

        // Append to eachother
        column1.appendChild(logo);
        column2.appendChild(name_input);
        column2.appendChild(team_name);
        column2.appendChild(remove_icon);
        player_li.appendChild(column1);
        player_li.appendChild(column2);
        output.appendChild(player_li);  // Add li to list
    }
    // IF NO LIST IS GIVEN, ASSUME WE HAVE TO PLACE THE TEAM'S ID INTO AN INPUT
    else
    {
        input_element.value = name;
        input_element.classList.add("disabled");
        if(result_output)   output.value = team_id;
        if(result_image)    document.getElementById(result_image).src = (image)? image : placeholder_image;
    }
}


/**
 * Fill list with given teams
 * @param {HTMLElement} list 
 * @param {string} input 
 * @param {JSON} teams 
 * @param {string} result_output 
 * @param {string} result_image 
 */
function fillList(list, input, teams, result_output=null, result_image=null)
{
    // Empty list
    list.innerHTML = '';
    let a_team_exists;
    
    // For each team
    for(let i=0; i<teams.length; i++)
    {
        let name = teams[i].name;
        
        // Check if the team is not already in the list
        let inputs = document.getElementById(result_output).getElementsByTagName("input");  // All inputs in list
        let this_team_exists = false;    // If team has been found in list
        
        for(var j=0; j<inputs.length; j++)
        {
            if(inputs[j].value == name)
            {
                a_team_exists = true;
                this_team_exists = true;
                break;
            }
        }
        if(this_team_exists) break;

        let team_li = document.createElement('li'); // Create li
        team_li.classList.add('collection-item', 'search-result', 'clickable');  // Add classes to li
        team_li.addEventListener("mousedown", selectTeam.bind(null, input, name, result_output, teams[i].id, result_image, teams[i].logo, list), false);

        let team_name = document.createTextNode(name);   // create textnode with name

        let image = document.createElement('img');
        image.classList.add('vertical-centre-image');   // Add class to image
        image.src = (teams[i].logo) ? teams[i].logo : placeholder_image;   // set image source

        team_li.appendChild(image);  // add image to li
        team_li.appendChild(team_name); // Add name into li
        list.appendChild(team_li);  // Add li to list
    }


    // If no team is found
    if( teams.length == 0 || (teams.length == 1 && a_team_exists) )
    {
        let team_li = document.createElement('li'); // Create li
        let text = document.createTextNode( "Geen teams gevonden." );   // create textnode with name

        team_li.classList.add('collection-item', 'search-result');  // Add classes to li
        team_li.appendChild(text); // Add name into li
        list.appendChild(team_li);  // Add entry to list
    }
}


/**
 * Get teams with name
 * @param {HTMLElement} list 
 * @param {string} input 
 * @param {string} result_output 
 * @param {string} result_image 
 */
function searchTeam(list, input, result_output=null, result_image=null, limit_id=null)
{
    // xmlhttp request and what to perform if successfully called
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function()
    {
        if(this.readyState == 4 && this.status == 200)
        {
            return fillList(list, input, JSON.parse(this.responseText), result_output, result_image);
        }
    }
    if(limit_id)
        xmlhttp.open("GET", "/teams/search?name=" + document.getElementById(input).value + '&id=' + limit_id, true);
    else
        xmlhttp.open("GET", "/teams/search?name=" + document.getElementById(input).value, true);
    xmlhttp.send();
}


/**
 * Looks for existing close buttons and adds eventlisteners
 */
function initRemoves()
{
    let removes = document.getElementsByClassName('close');
    for(let i=0; i < removes.length; i++)
    {
        removes[i].addEventListener("mousedown", removeList.bind(null, removes[i]), false);
    }
}


/**
 * Add event listeners
 */
function init()
{
    console.log('Tournament form javascript loaded!');
    initRemoves();

    let search_results = document.querySelectorAll('.search-results'); // Get all search results
    // console.log(search_results);
    // Iterate through em
    for(let i=0; i<search_results.length; i++)
    {
        let input = document.getElementById( search_results[i].dataset.searchInput );    // Get input that belongs to this search results.
        let search_image = search_results[i].dataset.searchImage;
        let search_output = search_results[i].dataset.searchOutput;

        // On typing in input
        input.addEventListener("keyup", function(e)
        {
            // Requests with 0 or 1 characters dont really work, so we make sure it has more than that in the value.
            if(input.value.length > 1)
            {
                searchTeam(search_results[i], search_results[i].dataset.searchInput, search_output, search_image, search_results[i].dataset.searchId );
            }
        });

        // On unfocus hide results
        input.addEventListener("blur", function(e)
        {
            search_results[i].classList.add("hide");
        });

        // On focus show results
        input.addEventListener("focus", function(e)
        {
            search_results[i].classList.remove("hide");
            input.classList.remove("disabled");
            if(search_image)    document.getElementById(search_image).src = "";
            if(search_output)   document.getElementById(search_output).value = "";
        });
    }
}
init();


