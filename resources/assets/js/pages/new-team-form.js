let placeholder_image = window.location.protocol + "//" + window.location.host + '/images/image-missing.png'

/**
 * Remove list where the given close butto nis in
 * @param {HTMLElement} close
 */
function removeList(close)
{
    close.parentNode.parentNode.remove();
}


/**
 * Select user for given input.
 * @param {string} input 
 * @param {string} name 
 * @param {string} result_output 
 * @param {integer} user_id 
 * @param {string} result_image 
 * @param {string} image 
 */
function selectUser(input, name=null, result_output=null, user_id=null, result_image=null, image=null, list=null)
{
    let input_element = document.getElementById(input);
    let output = document.getElementById(result_output);
    if(list)    list.innerHTML = "";

    // Empty input
    input_element.value = "";

    // IF A LIST IS GIVEN, ASSUME WE GOTTA ADD THE RESULT TO THE LIST
    if(output.nodeName == "UL")
    {
        // Create containers
        let player_li = document.createElement('li');
        let column1 = document.createElement('div');
        let column2 = document.createElement('div');
        // Add classes
        player_li.classList.add('collection-item', 'row'); 
        column1.classList.add('col', 's12', 'm2');
        column2.classList.add('col', 's12', 'm10');

        // Create input for name
        let name_input = document.createElement('input');
        name_input.type = 'checkbox';
        name_input.name = 'users[]';
        name_input.value = user_id;
        name_input.readOnly = true;
        name_input.checked = true;

        let user_name = document.createTextNode(name);   // create textnode with name

        let remove_icon = document.createElement('i');
        remove_icon.classList.add('mdi', 'mdi-close-circle', 'close', 'clickable');
        remove_icon.addEventListener("mousedown", removeList.bind(null, remove_icon), false);

        let avatar = document.createElement('img');
        avatar.classList.add('px52');   // Add class to image
        avatar.src = (image) ? image : placeholder_image;   // set image source

        // Append to eachother
        column1.appendChild(avatar);
        column2.appendChild(name_input);
        column2.appendChild(user_name);
        column2.appendChild(remove_icon);
        player_li.appendChild(column1);
        player_li.appendChild(column2);
        output.appendChild(player_li);  // Add li to list
    }
    // IF NO LIST IS GIVEN, ASSUME WE HAVE TO PLACE THE USER'S ID INTO AN INPUT
    else
    {
        input_element.value = name;
        input_element.classList.add("disabled");
        if(result_output)   output.value = user_id;
        if(result_image)    document.getElementById(result_image).src = (image)? image : placeholder_image;
    }
}


/**
 * Fill list with given users
 * @param {HTMLElement} list 
 * @param {string} input 
 * @param {JSON} users 
 * @param {string} result_output 
 * @param {string} result_image 
 */
function fillList(list, input, users, result_output=null, result_image=null)
{
    // Empty list
    list.innerHTML = '';
    let an_user_exists;
    
    // For each user
    for(let i=0; i<users.length; i++)
    {
        let name = users[i].first_name + ' ' + users[i].sur_name;
        
        // Check if the user is not already in the list
        let inputs = document.getElementById(result_output).getElementsByTagName("input");  // All inputs in list
        let this_user_exists = false;    // If user has been found in list
        
        for(var j=0; j<inputs.length; j++)
        {
            if(inputs[j].value == name)
            {
                an_user_exists = true;
                this_user_exists = true;
                break;
            }
        }
        if(this_user_exists) break;

        let user_li = document.createElement('li'); // Create li
        user_li.classList.add('collection-item', 'search-result', 'clickable');  // Add classes to li
        user_li.addEventListener("mousedown", selectUser.bind(null, input, name, result_output, users[i].id, result_image, users[i].avatar, list), false);

        let user_name = document.createTextNode(name);   // create textnode with name

        let image = document.createElement('img');
        image.classList.add('vertical-centre-image');   // Add class to image
        image.src = (users[i].avatar) ? users[i].avatar : placeholder_image;   // set image source

        user_li.appendChild(image);  // add image to li
        user_li.appendChild(user_name); // Add name into li
        list.appendChild(user_li);  // Add li to list
    }


    // If no user is found
    if( users.length == 0 || (users.length == 1 && an_user_exists) )
    {
        let user_li = document.createElement('li'); // Create li
        let text = document.createTextNode( "Geen teamleiders gevonden." );   // create textnode with name

        user_li.classList.add('collection-item', 'search-result');  // Add classes to li
        user_li.appendChild(text); // Add name into li
        list.appendChild(user_li);  // Add entry to list
    }
}


/**
 * Get users with name
 * @param {HTMLElement} list 
 * @param {string} input 
 * @param {string} result_output 
 * @param {string} result_image 
 */
function searchUser(list, input, result_output=null, result_image=null, limit_id=null)
{
    // xmlhttp request and what to perform if successfully called
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function()
    {
        if(this.readyState == 4 && this.status == 200)
        {
            return fillList(list, input, JSON.parse(this.responseText), result_output, result_image);
        }
    }
    if(limit_id)
        xmlhttp.open("GET", "/users/search?name=" + document.getElementById(input).value + '&id=' + limit_id, true);
    else
        xmlhttp.open("GET", "/users/search?name=" + document.getElementById(input).value, true);
    xmlhttp.send();
}


/**
 * Looks for existing close buttons and adds eventlisteners
 */
function initRemoves()
{
    let removes = document.getElementsByClassName('close');
    for(let i=0; i < removes.length; i++)
    {
        removes[i].addEventListener("mousedown", removeList.bind(null, removes[i]), false);
    }
}


/**
 * Add event listeners
 */
function init()
{
    console.log('Team form javascript loaded!');
    initRemoves();

    let search_results = document.querySelectorAll('.search-results'); // Get all search results
    // console.log(search_results);
    // Iterate through em
    for(let i=0; i<search_results.length; i++)
    {
        let input = document.getElementById( search_results[i].dataset.searchInput );    // Get input that belongs to this search results.
        let search_image = search_results[i].dataset.searchImage;
        let search_output = search_results[i].dataset.searchOutput;


        // On typing in input
        input.addEventListener("keyup", function(e)
        {
            // Requests with 0 or 1 characters dont really work, so we make sure it has more than that in the value.
            if(input.value.length > 1)
            {
                searchUser(search_results[i], search_results[i].dataset.searchInput, search_output, search_image, search_results[i].dataset.searchId );
            }
        });

        // On unfocus hide results
        input.addEventListener("blur", function(e)
        {
            search_results[i].classList.add("hide");
        });

        // On focus show results
        input.addEventListener("focus", function(e)
        {
            search_results[i].classList.remove("hide");
            input.classList.remove("disabled");
            if(search_image)    document.getElementById(search_image).src = "";
            if(search_output)   document.getElementById(search_output).value = "";
        });
    }
}
init();


