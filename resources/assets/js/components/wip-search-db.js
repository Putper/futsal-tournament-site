/**
 * @file SearchDB allows us to fill in a single input, or a list of multiple inputs, based on searches from an API.
 * @author <jasper@vanveenhuizen.nl>
 */
// Placeholder image in case no images are given
let placeholder_image = window.location.protocol + "//" +
    window.location.host + '/images/image-missing.png';


/**
 * Remove the list item the given element is in
 * @param {HTMLElement} element - An element located in the list
 */
function removeListItem(element)
{
    element.closest(li).remove();
}



/**
 * Looks for existing close buttons and adds eventlisteners
 */
function addEventListenersToRemoves()
{
    let remove_buttons = document.getElementsByClassName('close'); // Get all remove buttons
    for(let i=0; i < remove_buttons.length; i++)   // For each remove button
        remove_buttons[i].addEventListener( // Add event listener that removes list-item
            "mousedown",
            removeListItem.bind(null, remove_buttons[i]),
            false
        );  
}



/**
 * Add given result to list
 * @param {string} list_store_location_id - list to place the list-item in
 * @param {string} table - name of the table, which is used for the naming of the input.name
 * @param {int} item_id - ID of selected item
 * @param {string} item_name - name to show in the list-item
 * @param {string} image_path - path to image to show next to name.
 */
function selectItemList(table, item_id, item_name, image_path)
{
    list_store_location = document.getElementById(list_store_location_id);

    // Create containers
    let list_item = document.createElement('li');
    let column1 = document.createElement('div');
    let column2 = document.createElement('div');
    // Add classes
    list_item.classList.add('collection-item', 'row'); 
    column1.classList.add('col', 's12', 'm2');
    column2.classList.add('col', 's12', 'm10');

    // Create input
    let input = document.createElement('input');
    input.type = 'checkbox';
    input.name = table + '[]';
    input.value = item_id;
    input.readOnly = true;
    input.checked = true;

    let item_name_element = document.createTextNode(item_name);   // create textnode with name

    // Create a remove icon and make it delete the list-item when clicked
    let remove_icon = document.createElement('i');
    remove_icon.classList.add('mdi', 'mdi-close-circle', 'close', 'clickable');
    remove_icon.addEventListener("mousedown", removeListItem.bind(null, remove_icon), false);

    // Create an image to show next to the selcted list-item
    let item_image = document.createElement('img');
    item_image.classList.add('px52');   // Add class to image
    item_image.src = image_path;   // set image source

    // Append to eachother
    column1.appendChild(avatar);
    column2.appendChild(input);
    column2.appendChild(item_name_element);
    column2.appendChild(remove_icon);
    list_item.appendChild(column1);
    list_item.appendChild(column2);
    list_store_location.appendChild(list_item);  // Add li to list
}



/**
 * 
 * @param {string} selected_store_location_id - Input to store id
 * @param {tsring} input_id - input thats used to search
 * @param {int} item_id - id of selected item
 * @param {tsring} item_name - name of selected item
 * @param {tsring} image_id  - id of selected item
 * @param {tsring} image_path - image of selected item
 */
function selectItemInput(selected_store_location_id, input_id, item_id, item_name, image_id, image_path)
{
    let selected_store_location = document.getElementById(selected_store_location_id);
    let input = document.getElementById(input_id);
    let image = document.getElementById(image_id);

    input.value = item_name;    // set name as the value
    input.classList.add("disabled");    // make it look disabled/selected
    selected_store_location.value = item_id;    // Set the id as the value, which we'll need in the controller
    image.src = image_path;  // Set image to given image or placeholder image
}



/**
 * Fills list with given search results
 * @param {JSON} results - search results
 * @param {string} results_list_id - id of list that holds all results
 * @param {string} input_id - id of input
 * @param {string} selected_store_location_id - id of where to store selected search result.
 */
function fillResults(results, results_list_id, input_id, selected_store_location_id)
{
    let results_list = document.getElementById(results_list_id);
    let input = document.getElementById(input_id);
    let selected_store_location = document.getElementById(selected_store_location_id);

    // Empty list & input
    results_list.innerHTML = "";
    input.value = "";

    
    // For each found result
    for(let i=0; i<results.length; i++)
    {
        // set name. (if object has no key named name, assume we gotta combine a first and surname)
        let name = (results[i].name) ? results[i].name :
        results[i].first_name + ' ' + results[i].sur_name;

        let inputs = document.getElementById(result_output).getElementsByTagName("input");  // All inputs in list
    }

    // If a list is given as store location, we gotta add the selected
    // item to the list. If no list is given we select it in the input.
    // if(selected_store_location.nodeName == "UL")
}



/**
 * performs an xmlhttp request with search
 * @param {string} input_id - ID of input with value of search.
 */
function searchDB(input_id)
{
    let input = document.getElementById(input_id);

    // define XMLHttp request and what to run when it's ran
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function()
    {
        if(this.readyState == 4 && this.status == 200)  // if successful
        {
            return ;
        }
    }

    // Perform a request
    xmlhttp.open("GET", "/" + table + "/search?name=" + input_id.value);
    xmlhttp.send();
}



/**
 * Initialises plugin
 * @constructor
 */
function initSearchDB()
{
    console.log("Initialising SearchDB.");
    addEventListenersToRemoves();

    // Search results holds the info of everything required. So we get them all and set all the code up from there.
    let results_lists = document.querySelectorAll('.search-results');  // Get all search results
    for(let i=0; i<results_lists.length; i++)  // iterate through them
    {
        // Get values from dataset
        let input = document.getElementById( results_lists[i].dataset.searchInput );    // Get the search-input that belongs to this search results.
        let selected_store_image_location = results_lists[i].dataset.selectedStoreImageLocation;   // where to store image
        let selected_store_location = results_lists[i].dataset.selectedStoreLocation; // INPUT or UL to store search results


        // On typing in input
        input.addEventListener("keyup", function(e)
        {
            // Requests with 0 or 1 characters dont really work, so we make sure it has more than that in the value.
            if(input.value.length > 1)
                searchDB();
        });

        // On focus show results
        input.addEventListener("focus", function(e)
        {
            results_lists[i].classList.remove("hide");
            input.classList.remove("disabled");
            // Reset results to start new search :)
            if(selected_store_image_location)
                document.getElementById(search_image).src = "";
            if(selected_store_location)
                document.getElementById(search_output).value = "";
        });

        // On unfocus hide results
        input.addEventListener("blur", function(e)
        {
            results_lists[i].classList.add("hide");
        });
    }
}





