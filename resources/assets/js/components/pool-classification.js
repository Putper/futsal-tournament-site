/*
 *  Toggles between showing all pools of the tournament, and just the one the person is in.
 */
let pool_button = document.getElementById("pool-button");   // Button to press
let show_all = false;   // If it's currently showing all pools (true), or just the current one (false)


pool_button.onclick = function()
{
    let current_pool = document.getElementsByClassName('current-pool')[0];  // The current pool needs class 'current-pool'
    let other_pools = document.getElementsByClassName('other-pool');    // All other pools need the class 'other-pool'

    // If all are showing
    if(show_all)
    {
        current_pool.classList.remove("hide");

        for(let i=0; i<other_pools.length; i++)
        {
            other_pools[i].classList.add("hide");
        }

        pool_button.textContent = "Alle Poules";
        show_all = false;
    }
    // if just the current is showing
    else
    {
        current_pool.classList.add("hide");

        for(let i=0; i<other_pools.length; i++)
        {
            other_pools[i].classList.remove("hide");
        }
        
        pool_button.textContent = "Eigen Poule";
        show_all = true;
    }
};