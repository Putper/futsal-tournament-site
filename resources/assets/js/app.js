/**
 * Scales all image with 'scale-image' class with a max size of data-image-size
 */
function scale_images()
{
    let images = document.getElementsByClassName('scale-image');    // get all images
    for(let i=0; i<images.length; i++)  // for each image
    {
        let requested_size = images[i].dataset.imageSize;   // get the max size the image has to be
        
        // get image sizes
        let width = images[i].naturalWidth;
        let height = images[i].naturalHeight;
        console.log(images[i]);
        
        console.log(width);
        console.log(height);

        // set image size based on proportions of image
        if(width < height)  // if image is more high than width
        {
            images[i].style.height = requested_size + 'px';     // set height as requested size
            images[i].style.width = 'auto';     //set width as auto
        }
        else    // if image is more wide than high, or the same
        {
            images[i].style.height = 'auto';    // set height as auto
            images[i].style.width = requested_size + 'px';  // set width as requested size
        }
    }
}

// ON DOCUMENT READY
document.addEventListener('DOMContentLoaded', function()
{
    M.AutoInit();   // Auto init materialize components

    // If a modal has the class modal-default-open, open it on load.
    let modals = document.getElementsByClassName('modal modal-default-open');
    for(let i=0; i<modals.length; i++)
    {
        M.Modal.getInstance(modals[i]).open();
    }

    scale_images();
});


/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// require('./bootstrap');

// window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));

// const app = new Vue({
//     el: '#app'
// });

