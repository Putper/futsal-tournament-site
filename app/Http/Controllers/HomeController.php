<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Home Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and updating.
    |
    */

    /**
     * redirect correctly
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return view('/pages/tournament');
        if(Auth::check())
        {
            $user = Auth::user();
            $tournament = $user->tournaments(true)->first();
            if($tournament)
                return redirect(route('tournament', $tournament->id));
            else
            return redirect(route('tournaments'));
        }
        else                return redirect(route('login'));
    }
}
