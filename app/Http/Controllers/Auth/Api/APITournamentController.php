<?php

namespace App\Http\Controllers;
namespace App\Http\Controllers\Auth\Api;
use App\Http\Controllers\Pages\TournamentController;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Tournament;
use App\Models\Team;
use App\Models\Result;
use App\Models\Role;
use JWTFactory;
use JWTAuth;
use Validator;
use Response;
use Illuminate\Auth\Events\Registered;

use Illuminate\Http\Request;

class APITournamentController extends Controller
{
    /**
     * Display a listing of Toernaments.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {

         return Tournament::all();
        //Get and combine matches beloning to tournament
        


     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Tournament $tournament)
    {
        return $tournament;
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    /**
     * View the tournament with the given ID
     * @param integer $id
     */
    public function viewTournament($tournament_id)
    {
        $token = JWTAuth::getToken();
        $payload = JWTAuth::getPayload($token)->toArray();
        // dd($payload);
        $tournament_controller = new TournamentController;
        $data =[ $tournament_controller->viewTournament($tournament_id, $payload['sub'], false)]; 
        $upcoming_matches = [ $tournament_controller->viewTournament($tournament_id, $payload['sub'], false)][0]['upcoming_matches']; 
        
            $teams = array();
                foreach( $upcoming_matches as $key => $match ){
                $start = $match['start'];
                $field = $match['field'];
                
                $team1 = Team::find(Result::find($match['result1_id'])->team_id);
                $team2 = Team::find(Result::find($match['result2_id'])->team_id);
                // $teams_combined = array($team1, $team2);
                $upcoming_matches[$key]["team1"] = $team1;
                $upcoming_matches[$key]["team2"] = $team2;
                }

                // $return = array($data, $upcoming_matches);
                return $upcoming_matches;

                // return array(['match'=> $upcoming_matches, 'upcomig'=>$teams,]);
    }

    public function viewFirstmatch($tournament_id)
    {
        $token = JWTAuth::getToken();
        $payload = JWTAuth::getPayload($token)->toArray();
        // dd($payload);
        $tournament_controller = new TournamentController;
        $data =[ $tournament_controller->viewTournament($tournament_id, $payload['sub'], false)]; 
        
        
                return $data;

                // return array(['match'=> $upcoming_matches, 'upcomig'=>$teams,]);
    }
}

