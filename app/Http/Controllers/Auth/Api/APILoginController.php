<?php

namespace App\Http\Controllers\Auth\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use JWTFactory;
use JWTAuth;
use App\User;
use Illuminate\Support\Facades\Auth;

class APILoginController extends Controller

/*
    |--------------------------------------------------------------------------
    | APILoginController
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users return token or json messege error 
    | 
    |
    */
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [//required 
            'email' => 'required|string|email|max:255',//email is required
            'password'=> 'required'//password is required
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        $credentials = $request->only('email', 'password');
        try {
            if (! $token = JWTAuth::attempt($credentials)) {//if login in not succeded the user will get json messege erro 401.
                return response()->json(['Error' => 'Email of watchwoord klop niet.'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['Error' => 'could_not_create_token'], 500);
        }
        return response()->json(compact('token'));//if login is succeded user will get token
    }
}
