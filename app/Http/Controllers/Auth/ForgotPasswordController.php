<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\PasswordReset;
use App\Models\User;
use App\Mail\Forgot;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails.
    |
    */

    use SendsPasswordResetEmails;



    /**
     * reest an user's password
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function forgotPassword(Request $request)
    {
        $user = User::where('email', $request->email)->get()->first();  // Get user with email
        
        if($user && $user->last_seen)   // make sure there's an user. cant really reset an user's password if he doesn't exist :P
        {
            $token = $this->generateForgotPasswordToken();  // Generate a token
            $password_reset = PasswordReset::where('email', $user->email)->first(); // Check if user already has a password_reset

            // If he doesn't have one yet, create a password_reset for the user. Else just update the one we found.
            if(!$password_reset)
            {
                $password_reset = new PasswordReset();
                $password_reset->email = $user->email;
            }
            $password_reset->token = $token;
            $password_reset->created_at = time();
            $password_reset->save();

            // Send mail with token
            Mail::to($user->email)->send(new Forgot( route('reset.password.token', $token) ));

            return redirect(route('forgot.password.route'));
        }
        else    return \Redirect::back()->withErrors(['Dit email is niet gekoppeld aan een geregistreerde gebruiker.']);
        abort(404); // TODO: proper erroring. email is not bound to an account



    }


    /**
     * Generates a random string with the given length.
     * 
     * @param   string  $length
     * @return string
     */
    protected function generateForgotPasswordToken($length = 64)
    {
        $character_array = array_merge(range('a','z'), range('A', 'Z'), range('0','9'));
        $max = count($character_array)-1;
        $url = "";

        // Generate url
        for ($i = 0; $i < $length; $i++)
        {
            $random_character = mt_rand(0, $max);
            $url .= $character_array[$random_character];
        }
        return $url;
    }
}
