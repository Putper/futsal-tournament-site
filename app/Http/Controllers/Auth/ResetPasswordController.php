<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use \Illuminate\Http\Request;
use App\Models\PasswordReset;
use App\Models\User;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/';


    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function ResetPassword(Request $request)
    {
        if(Auth::Check())
            $user = Auth::User();
        else
        {
            if(!$request->token)    return \Redirect::back()->withErrors(['Je moet ingelogd zijn of een token meegeven.']);

            $reset_password = PasswordReset::where('token', $request->token)->first();
            if(!$reset_password)    return \Redirect::back()->withErrors(['Wachtwoord Reset Link is niet correct of verlopen.']);

            $user = User::where('email', $reset_password->email)->first();
            if(!$user)  return \Redirect::back()->withErrors(['Token hoort bij geen gebruiker.', 'Neem contact op met een beheerder. ERROR:resetpassword01']);
        }

        $user->password = Hash::make($request->password);   // Hash password
        $user->save();  // update to database
        // return $this->showResetForm($request, null, true);
        return redirect(route('home'));
    }


    /**
     * Display the password reset view for the given token.
     * 
     * @param string|null  $token
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getReset($token = null)
    {
        if (is_null($token))
        {
            throw new NotFoundHttpException;    
        }
    
        return redirect(route('reset.password')->with('token', $token));
    }


    /**
     * Display the password reset view for the given token or logged in user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $token
     * @param   bool|null   $success
     * @return \Illuminate\Http\RedirectResponse
     */
    public function showResetForm(Request $request, $token=null, $success=null)
    {
        if(Auth::check())   // if logged in, use your own email
            $email = Auth::user()->email;
        elseif($request->email) // if email is given in the request, use that
            $email = $request->email;
        elseif($token)    // Else get email from password reset
        {
            $email = PasswordReset::where('token', $token)->first()->email;
        }
        else    return \Redirect::back()->withErrors(['Je bent niet ingelogd en hebt geen email of token aangegeven.']);

        return view('pages/auth/reset-password',
            ['token' => $token, 'email' => $email, 'success' => $success]
        );
    }

    /**
     * Generates a random string with the given length.
     * 
     * @param   string  $length|null
     * @return string
     */
    private function generateResetPasswordToken($length = 64)
    {
        $character_array = array_merge(range('a','z'), range('A', 'Z'), range('0','9'));
        $max = count($character_array)-1;
        $token = "";

        // Generate url
        for ($i = 0; $i < $length; $i++)
        {
            $random_character = mt_rand(0, $max);
            $token .= $character_array[$random_character];
        }
        return $token;
    }
}
