<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\UserController;

class UpdateUserController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users who got invited
    | as well as their validation and updating.
    |
    */

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';


    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateUser(Request $request)
    {
        if(!Auth::Check())  return \Redirect::back()->withErrors(['Je moet ingelogd zijn om je profiel aan te passen.']);  // If logged in
        $this->updateValidator($request->all())->validate();  // Validate data
        
        $user = Auth::User();   // Get user
        
        // $user = User::find($user->id);  // Find user with that token (Eloquent)

        $user->fill($request->all());   // Fill user with given data

        // If avatar is provided, upload it.
        if ($request->hasFile('avatar'))
        {
            $image_controller = new ImageController;
            $image_path = $image_controller->uploadImage($request->file('avatar'), 'avatars', (string)$user->id);
            if($image_path) $user->avatar = $image_path;
        }

        $user->save();  // update to database

        return redirect(route('profile')); // Redirect to home
    }


    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function register(Request $request)
    {  
        $this->registerValidator($request->all())->validate();  // Validate data
        // die($request->avatar);

        $user_controller = new UserController;
        $user = $user_controller->getUserWithToken($request->token);   // Get user with the given register token
        
        if($user == null)   return \Redirect::back()->withErrors(['Deze registreren link is incorrect of verlopen']);

        $user = User::find($user->id);  // Find user with that token (Eloquent)

        $user->fill($request->all());   // Fill user with given data
        $user->password = Hash::make($request->password);   // Hash password
        $user->register_token = null;   // Reset register token
        $user->last_seen = new \DateTime(); // set initial last_seen

        // If avatar is provided, upload it.
        if ($request->hasFile('avatar'))
        {
            $image_controller = new ImageController;
            $image_path = $image_controller->uploadImage($request->file('avatar'), 'avatars', (string)$user->id);
            if($image_path) $user->avatar = $image_path;
        }

        $user->save();  // update to database
        Auth::loginUsingId($user->id);  // Login user

        return redirect(route('home')); // Redirect to home
    }
    

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function registerValidator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:199',
            'sur_name' => 'required|string|max:199',
            // 'avatar' => [
            //     'nullable',
            //     Rule::dimensions()->maxWidth(512)->maxHeight(512)->ratio(1 / 1),
            // ],
            'avatar' => 'image',
            // 'avatar' => 'dimensions:ratio=1/1,min_width=512,min_height=512',
            'password' => 'required|string|min:5|confirmed',
        ]);
    }
    

    /**
     * Get a validator for an incoming update user request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function updateValidator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'string|max:199',
            'sur_name' => 'string|max:199',
            'avatar' => 'image',
        ]);
    }

    /**
     * redirect correctly
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function editProfilePage()
    {        
        if(!Auth::check())  return \Redirect::back()->withErrors(['Je moet ingelogd zijn om je profiel aan te passen.']);

        return view("/pages/auth/update-user", ['registering' => 0, 'user' => Auth::User()]);
    }


    /**
     * redirect correctly / load register page for user with given register token.
     *
     * @param   string  $token
     * @return \Illuminate\Http\RedirectResponse
     */
    public function registerPage($token)
    {
        $user = DB::table('user')->where('register_token', $token)->first();    // Find user with matched token
        if($user)   // If there's an user with a matched token
        {
            return view("/pages/auth/update-user", ['registering' => 1, 'register_token' => $token]);  // Go to registering page.
        }
        else    return \Redirect::back()->withErrors(['Deze Registratie Link is incorrect of verlopen']);
    }
}
