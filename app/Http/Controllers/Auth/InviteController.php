<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use App\Mail\Invite;

class InviteController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Invite Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the inviting of new users as well as their
    | validation and creation.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect after inviting.
     *
     * @var string
     */
    protected $redirectTo = '/uitnodigen';


    /**
     * Handle an invite
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function invite(Request $request)
    {
        $this->validator($request->all())->validate();  // Validate filled info

        if(!Auth::Check())  return \Redirect::back()->withErrors(['Je moet ingelogd zijn om mensen uit te nodigen.']);
        $user = Auth::user();   // User performing the invite

        $role = Role::find($user->role_id); // His role
        if($role->permission <= Role::find($request->role)->permission)     return \Redirect::back()->withErrors(['Je kan geen gebruikers uitnodigen met een rol hoger of gelijk aan jezelf.' ]);
        
        $invited_user = User::where('email', $request->email)->get()->first();   // Find user with the given email
        $invited_user_object = null;

        // If the user we're trying to invite doesn't have an account yet; create one for him.
        if(!$invited_user)
            event(new Registered( $invited_user = $this->create($request->all() )));  // Create user
        // Else if he does have an account, and has already been registered; return error
        elseif($invited_user->last_seen !== null)
            abort(404);
        // Else assume account already exists, has already been invited, but needs another invite.
        else
        {
            $invited_user_object = User::find($invited_user->id);
            if(!$invited_user_object)   return \Redirect::back()->withErrors(['Gebruiker bestaat, is nog niet geregistreerd, maar ook nog niet uitgenodigd.', 'Neem contact op met een beheerder! ERROR:invite01']);
            $invited_user_object->register_token = $this->generateRegisterToken();
            $invited_user_object->save();
        }
        
        // Send mail with invite
        if($invited_user_object)
            Mail::to($invited_user->email)->send(new Invite(route('register', ['token' => $invited_user_object->register_token])));
        else
            Mail::to($invited_user->email)->send(new Invite(route('register', ['token' => $invited_user->register_token])));
        
        // check for failures
        if (Mail::failures()) {
            return \Redirect::back()->withErrors(['Er is iets verkeerd gegaan bij het versturen van een mail', 'Neem contact op met een beheerder! ERROR:invite02']);
        }

        if($request->request_url)
        {
            if($invited_user_object)
                $request_url = $invited_user_object->register_token;
            else
                $request_url = $invited_user->register_token;
            return redirect(route('invite.with.url', $request_url));    // redirect with request url
        }
        return redirect($this->redirectPath()); // Redirect
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|string|email|max:199',
            'role' => 'required|int|max:4',
        ]);
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        return User::create([
            'email' => $data['email'],
            'role_id' => $data['role'],
            'register_token' => $this->generateRegisterToken(),
        ]);
    }


    /**
     * Generates a random string with the given length.
     * 
     * @param   string  $length
     * @return string
     */
    protected function generateRegisterToken($length = 64)
    {
        $character_array = array_merge(range('a','z'), range('A', 'Z'), range('0','9'));
        $max = count($character_array)-1;
        $url = "";

        // Generate url
        for ($i = 0; $i < $length; $i++)
        {
            $random_character = mt_rand(0, $max);
            $url .= $character_array[$random_character];
        }
        return $url;
    }
    
    
    /**
     * redirect correctly
     *
     * @param string|null $url
     * @return \Illuminate\Http\RedirectResponse
     */
    public function index($url = null)
    {
        if(!Auth::check())  return \Redirect::back()->withErrors(['Je moet ingelogd zijn om mensen uit te nodigen.']);
        
        $user = Auth::user();   // Get user
        $user_role = Role::find($user->role_id);

        if( $user && $user_role->permission >= 30)   // If user is logged in and has the correct role to invite people
        {
            $roles = Role::where('permission', '<', $user_role->permission)->get()->all();  // Get all roles that the user is allowed to invite.    // TODO: PUT IN MODEL
            
            return view('/pages/auth/invite')->with('roles', $roles)->with('url', $url);  // redirect to invite page with the roles
        }
        else    return \Redirect::back()->withErrors(['Je hebt niet de juiste permissies om een gebruiker uit te nodigen.']);
    }
}
