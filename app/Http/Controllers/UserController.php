<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;
use \Illuminate\Http\Response;

class UserController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | User Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles user-related things. Excluding Authorisation &
    | Authentication, this is all done in the controllers in the /Auth/ folder.
    |
    */


    /**
     *  Search user based on name
     * 
     * @param string|null $name
     * @param int|null $role_id
     * @return \App\Models\User
     */
    public function search($name=null, $role_id=null)
    {
        // if no parameters were given, its from the request
        $name = ($name) ? $name : request()->name;

        // If Id is given, search user with specific role_id.
        if(request()->id || $role_id)
        {
            // if no parameters were given, its from the request
            $role_id = ($role_id) ? $role_id : request()->id;
            // $role = Role::find($role_id);
            // $permission = $role->permission;

            return response()->json(
                User::searchName($name, $role_id)    ->distinct()->get()
            );
        }
        else
        {
            return response()->json(
                User::searchName($name)    ->distinct()->get()
            );
        }
    }


    /**
     * Generate user data
     * 
     * @param   int|null  $amount
     * @param   string|null $region
     * @return array
     */
    public function generateNames(int $amount = 1, $region="England")
    {
        // UInames provides random surnames that we use as team names
        $json = json_decode(file_get_contents("https://uinames.com/api/?amount=$amount&region=$region"), true);
        return $json;
    }


    /**
     * Get ID from random user, possible with specified id
     * 
     * @param integer|null $role
     * @return integer
     */
    public function getRandomUserId($role=null)
    {
        $user = new User();
        return $user->getRandomUserId($role);
    }

    
    /**
     * Get user with given register token
     * 
     * @param   string  $token
     * @return App\Models\User
     */
    public function getUserWithToken($token)
    {
        $user = new User();
        return $user->getUserWithToken($token);
    }
}
