<?php

namespace App\Http\Controllers\Pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Models\Tournament;
use App\Models\Pool;
use App\Models\Team;
use App\Models\Match;
use App\Models\Result;
use App\Models\Role;
use App\Models\User;
use Validator;
use DateTime;

class TournamentController extends Controller
{
    /**
     * Generate matches for a given pool
     * 
     * @param array[App\Models\Pool] $pools
     * @return \Illuminate\Http\Response
     */
    protected function generateMatches($pools)
    {
        // For each pool
        foreach($pools as $pool)
        {
            $teams = $pool->teams;
            $teams_size = sizeof($teams);

            // Foreach teams in this pool
            foreach($teams as $team)
            {
                for($i=0; $i<$teams_size-1; $i++ )
                {
                    $team_1 = $teams[$i]->id;
                    $team_2_index = $teams_size-1-$i;
                    // dd($teams);
                    $team_2 = $teams[$team_2_index]->id;
                    // dd($team_2);
                    if($team_1 == $team_2)  $team_2 == 1;    // if team has to play against itself, put it against "bye" instead.

                    // Generate results
                    $result1 = new Result();
                    $result1->team_id = $team_1;
                    $result1->save();
                    $result2 = new Result();
                    $result2->team_id = $team_2;
                    $result2->save();

                    // Generate match
                    $match = new Match();
                    $match->round = $i+1;
                    $match->result1_id = $result1->id;
                    $match->result2_id = $result2->id;
                    $match->pool_id = $pool->id;   
                    $match->has_ended = 0;
                    $match->save();
                }
            }
        }
        return redirect(route('home'));
    }


    /**
     * Generate pools for this tournament
     * 
     * @param App\Models\Tournament $tournament
     * @param array[int] $teams
     * @param int $pools
     * @return \Illuminate\Http\Response
     */
    protected function generatePools($tournament, $teams, $pools)
    {
        $teams_iteration = 0;   // Where in the $teams we are
        $teams_per_pool = sizeof($teams)/$pools;    // How many teams each pool has.
        $teams_too_many = 0;    // How many teams are leftover (to compensate for uneven team/pool balance)
        $added_pools = [];  // List of all the pools that have been generated

        // Where to start the pool number counting
        $pool_count = Tournament::find($tournament->id)->poolNumber();
        
        // Get how many players are leftover (for even pools/teams balance)
        $rounded_teams_per_pool = floor($teams_per_pool);
        if($rounded_teams_per_pool !== $teams_per_pool)
            $teams_too_many = round(($teams_per_pool - $rounded_teams_per_pool)*$pools);  // Calculate how many players are leftover
            
        // For each pool, create a pool with teams.
        for($i=0; $i<$pools; $i++)
        {
            // Create new pool
            $pool = new Pool();
            $pool->number = $i+1 + $pool_count;
            $pool->tournament_id = $tournament->id;
            $pool->finished = 0;

            // Check if this pool needs an extra team to compensate for uneven team/pool balance.
            if($teams_too_many > 0)
            {
                $extra = 1;
                $teams_too_many -= 1;
            }
            else    $extra = 0;
            
            $teams_in_pool = [];    // array to store team IDs
            for($j=0; $j<$rounded_teams_per_pool+$extra; $j++) // For each teams amount
            {
                $teams_in_pool[] = $teams[$teams_iteration]; // add team to array
                $teams_iteration++;
            }

            $pool->save();
            $pool->teams()->sync($teams_in_pool); // Attach teams to pool
            $pool->save();

            $added_pools[] = $pool; // Add pool to the list that holds all pools that have been created this session
        }

        return redirect(route('tournament', $tournament->id));
        // return  $this->generateMatches($added_pools);

    }



    /**
     * Create or update a tournament
     *
     * @param  \Illuminate\Http\Request  $request
     * @param   bool|null   $udpate
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $update=false)
    {
        // dd($request->all());
        $this->validator($request->all())->validate();  // Validate filled info
        
        if(!Auth::Check())  // if not logged in return error
            return \Redirect::back()->withErrors(['Je moet ingelogd zijn.']); 

        $user = Auth::user();   // Logged in user
        $role = Role::Find($user->role_id); // Their role

        // If updating a tournament, get tournament from id Else create a new tournament.
        if($update)
        {
            $tournament = Tournament::find($request->id);
            if(!$tournament)    return \Redirect::back()->withErrors(['Toernooi niet gevonden.']);  // If no tournament is found, return error.
        }
        else
            $tournament = new Tournament();

        if($role->permission >= 50)  // If user is an organiser or higher, they are allowed to alter tournaments :)
        {
            // Set parameters
            $tournament->name = $request->name;
            $tournament->start_date = $request->start_date;
            $tournament->end_date = $request->end_date;
            // Save tournament to DB
            $tournament->save();

            // generate pools
            if($request->teams && $request->pools_amount)
                return $this->generatePools($tournament, $request->teams, $request->pools_amount);

            // Redirect back
            return redirect(route('tournaments'));    // TODO: Message that tournament has been created
        }
        else    return \Redirect::back()->withErrors(['Je hebt hier niet de juiste permissies voor.']);
    }
    

    /**
     * Edit a tournament
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        return $this->create($request, true);
    }
    

    /**
     * Delete a tournament with given id
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function remove(int $id)
    {
        if(!Auth::Check())  return \Redirect::back()->withErrors(['Je moet ingelogd zijn om een toernooi te verwijderen.']);

        $user = Auth::User();
        $tournament = Tournament::find($id);
        if(!$tournament)    return \Redirect::back()->withErrors(['Toernooi niet gevonden.']);

        // organisers can remove tournaments.
        if($user->role->permission >= 50)
        {
            $tournament->delete();
            return redirect(route('tournaments'));
        }
        else    return \Redirect::back()->withErrors(['Je hebt niet de juiste permissies om een toernooi te verwijderen.']);
    }
    

    /**
     * Delete a tournament from post request
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function removeByRequest(Request $request)
    {
        return $this->remove($request->id);
    }


    /**
     * Get a validator for an incoming request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,
        [
            'name' => 'required|string|max:199',
            'start_date' => 'date|required',
            'end_date' => 'date|nullable',
            'mott_id' => 'integer|nullable',
        ]);
    }



    /**
     * View the tournament with the given ID
     * @param integer $id
     * @param integer|null $user_id
     * @param   bool|null   $redirect
     * @param boolean $redirect
     */
    public function viewTournament($id, $user_id=null, $redirect=true)
    {
        if($user_id)
        {
            $user = User::find($user_id);
            if(!$user)  return \Redirect::back()->withErrors(['Gebruiker niet gevonden']);
        }
        else
        {
            if(!Auth::check())  return \Redirect::back()->withErrors(['Je moet ingelogd zijn om een toernooi te bekijken.']);
            $user = Auth::user();
        }
            
        $tournament = Tournament::find($id);
        if(!$tournament)    return \Redirect::back()->withErrors(['Toernooi niet gevonden.']);
        
        $user_permission = $user->role->permission;
        $team1=null;
        $team2=null;
        
        // upcoming-match.blade.php
        $my_first_match = $tournament->myFirstMatch($user->id);   // Get first match
        

        if($my_first_match)
        {
            $team1 = $my_first_match->result1->team;  // Get team 1 of the match
            $team2 = $my_first_match->result2->team;  // Get team 2 of the match
        }

        $pools = $tournament->pools()->get()->all();    // Get all pools

        // If the player is in a pool, $my_pool has to be that pool. Else the first pool has to be $my_pool.
        if($user_id)
            $my_pool = $tournament->myPools(null, $user_id)->first();
        else
            $my_pool = $tournament->myPools()->first();

        if(!$my_pool && !empty($pools))   $my_pool = $pools[0];
        
        if($pools && $my_pool)
            array_unshift($pools, $my_pool);    // Place the pool the player is in to the beginning of the array.

        $current_date = new DateTime();  // Get current date for comparison
        $current_date = $current_date->format('Y-m-d H:i:00');
        // dd($current_date);
        

        // Get current matches
        $current_matches = $tournament->matches()->where('has_ended', 0)->where('start', '<=', $current_date)->get();   // get current matches through pools
        $current_matches_2 = $tournament->extraMatches()->where('has_ended', 0)->where('start', '<=', $current_date)->get();    // Get current matches directly
        $current_matches = $current_matches->merge($current_matches_2)->all();  // Merge them together

        if(sizeof($current_matches) > 1)   // if therea are matches, sort them
        {
            foreach ($current_matches as $key => $match)    // for each match
                $current_sort[$key] = strtotime($match['start']);   // Convert start time to a timestamp and store it in an array
            array_multisort($current_sort, SORT_DESC, $current_matches); // Sort the matches based on the timestamps
            $current_matches = array_reverse($current_matches);  // Reverse it so that the earliest matches are first
        }


        $finished_matches = $tournament->matches()->where('has_ended', 1)->get();
        $finished_matches_2 = $tournament->extraMatches()->where('has_ended', 1)->get();
        $finished_matches = $finished_matches->merge($finished_matches_2)->all();
        if(sizeof($finished_matches) > 1)   // if therea are matches, sort them
        {
            foreach ($finished_matches as $key => $match)    // for each match
                $finished_sort[$key] = strtotime($match['start']);   // Convert start time to a timestamp and store it in an array
            
            array_multisort($finished_sort, SORT_DESC, $finished_matches); // Sort the matches based on the timestamps
            $finished_matches = array_reverse($finished_matches);  // Reverse it so that the earliest matches are first
        }


        $upcoming_matches = $tournament->matches()->where('has_ended', 0)->where('start', '>', $current_date)->get();
        $upcoming_matches_2 = $tournament->matches()->where('has_ended', 0)->where('start', null)->get();
        $upcoming_matches_3 = $tournament->extraMatches()->where('has_ended', 0)->where('start', '>', $current_date)->get();
        $upcoming_matches_4 = $tournament->extraMatches()->where('has_ended', 0)->where('start', null)->get();
        $upcoming_matches = $upcoming_matches->merge($upcoming_matches_2)->merge($upcoming_matches_3)->merge($upcoming_matches_4)->all();

        if(sizeof($upcoming_matches) > 1)   // if therea are matches, sort them
        {
            foreach ($upcoming_matches as $key => $match)    // for each match
                $upcoming_sort[$key] = strtotime($match['start']);   // Convert start time to a timestamp and store it in an array
            array_multisort($upcoming_sort, SORT_DESC, $upcoming_matches); // Sort the matches based on the timestamps
            $upcoming_matches = array_reverse($upcoming_matches);  // Reverse it so that the earliest matches are first
        }

        if($redirect)
        {
            return view('pages/tournament',
            [
                'id' => $id,
                'permission' => $user_permission,
                'match' => $my_first_match,
                'team1' => $team1,
                'team2' => $team2,
                'pools' => $pools,
                'current_matches' => $current_matches,
                'upcoming_matches' => $upcoming_matches,
                'finished_matches' => $finished_matches,
            ]);
        }
        else
        {
            $data = array(
                'id' => $id,
                'permission' => $user_permission,
                'match' => $my_first_match,
                'team1' => $team1,
                'team2' => $team2,
                'pools' => $pools,
                'current_matches' => $current_matches,
                'upcoming_matches' => $upcoming_matches,
                'finished_matches' => $finished_matches,
            );
            return $data;
        }
    }



    /**
     * redirect to tournaments list page
     *
     * @return \Illuminate\Http\Response
     */
    public function tournamentsList()
    {
        if(!Auth::Check())  return \Redirect::back()->withErrors(['Je moet ingelogd zijn een toernooien te bekijken']);

        $user = Auth::User();
        $current_date = date('Y-m-d');  // Get current date for comparison
        
        // TODO: I dont believe doing 3 queries is the best way to do this.
        // Possibly find a way to do this an ::all() query and collections https://laravel.com/docs/5.6/collections
        // if($user->role->permission >= 50)
        // {
            $current = Tournament::whereDate('start_date', '<=', $current_date)->whereDate('end_date', '>=', $current_date)->get(); // All current tournaments dont have a mott yet, and the start date must have already happened.
            $upcoming = Tournament::whereDate('start_date', '>', $current_date)->get(); // All upcoming tournaments have a start date in the future
            $finished = Tournament::whereDate('start_date', '<', $current_date)->whereDate('end_date', '<', $current_date)->get();    // All tournaments that happened have a startdate in the past and have a mott set.
            $can_edit = true;
        // }
        // else
        if($user->role->permission < 50)
        {
            // $tournaments = $user->tournaments();
            
            // $current = Tournament::where('mott_id', null)->whereDate('start_date', '<=', $current_date)->get(); // All current tournaments dont have a mott yet, and the start date must have already happened.
            // $upcoming = Tournament::whereDate('start_date', '>', $current_date)->get(); // All upcoming tournaments have a start date in the future
            // $finished = Tournament::whereNotNull('mott_id')->whereDate('start_date', '<', $current_date)->whereDate('end_date', '<', $current_date)->get();    // All tournaments that happened have a start date in the past and have a mott set.
            $can_edit = false;
        }
        
        return view('pages/tournaments',
        [
            'current_tournaments' => $current,
            'upcoming_tournaments' => $upcoming,
            'finished_tournaments' => $finished,
            'can_edit' => $can_edit
        ]);
    }



    /**
     * redirect to page that allows user to create a tournament
     *
     * @return \Illuminate\Http\Response
     */
    public function showNewForm()
    {
        if(Auth::Check())
        {
            $user = Auth::User();
            if($user->role->permission >= 50)
            {
                return view('/pages/cud-tournament')->with('creating', true);
            }
            else return \Redirect::back()->withErrors(['Je hebt niet de juiste permissies om een toernooi aan te maken.']);
        }
        else return \Redirect::back()->withErrors(['Je moet ingelogd zijn om een toernooi te bekijken']);
    }


    /**
     * redirect to page that allows user to edit a tournament
     *
     * @param   int $id
     * @return \Illuminate\Http\Response
     */
    public function showEditForm(int $id)
    {
        if(Auth::Check())
        {
            $user = Auth::User();
            $tournament = Tournament::find($id);

            if(!$tournament)    return \Redirect::back()->withErrors(['Toernooi niet gevonden.']);  // Throw error if tournament not found

            // The team leader OR organisers are allowed to edit teams
            if($user->role->permission >= 50)
            {
                // dd($pools);
                return view('/pages/cud-tournament')
                    ->with('creating', false)
                    ->with('tournament', $tournament);
            }
            else    return \Redirect::back()->withErrors(['Je hebt niet de juiste permissies om een toernooi aan te passen.']); // Throw error if not the correct permissions to edit
        }
        else    return \Redirect::back()->withErrors(['Je moet ingelogd zijn om een toernooi te bekijken.']);   // Throw error if not logged in

        return redirect(route('home'));
    }
}