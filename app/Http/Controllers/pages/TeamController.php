<?php

namespace App\Http\Controllers\Pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\UserController;
use App\Models\Team;
use App\Models\User;
use App\Models\Role;
use Validator;

class TeamController extends Controller
{
    /**
     *  Search team based on name
     * 
     * @param   string|null $name
     * @return \App\Models\Team
     */
    public function search($name=null)
    {
        // if no parameters were given, its from the request
        $name = ($name) ? $name : request()->name;

        return response()->json(
            Team::searchName($name)    ->distinct()->get()
        );
    }



    /**
     * redirect to teams page
     *
     * @return \Illuminate\Http\Response
     */
    public function teamsList()
    {
        if(!Auth::check())  return \Redirect::back()->withErrors(['Je moet ingelogd zijn om teams te bekijken.']);
        
        $teams = Team::all();
        $user = Auth::User();

        // People with permission 50 or higher can edit all.
        $edit_all = ($user->role->permission >= 50);

        // Get all teams the user is leader of
        $leading_teams = Team::where('leader_id', $user->id)->get(['id']);
        $leading_teams_ids = [];
        foreach($leading_teams as $team)    $leading_teams_ids[] = $team->id;


        return view('pages/teams',
        [
            'teams' => $teams,
            'leading_teams' => $leading_teams_ids,
            'edit_all' => $edit_all
        ]
        );
    }



    /**
     * Check if a specified player is in a specified team.
     * 
     * @param int $player_id
     * @param int $team_id
     * @return bool
     */
    private function isInTeam($player_id, $team_id)
    {
        $team = Team::find($team_id);
        $is_in_team = $team->players()->where('id', $player_id)->get()->first();
        if($is_in_team)    return true;
        else    return false;
    }


    /**
     * Create or update an team. If updating; id must be in request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param   bool|null   $update
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $update=false)
    {
        $this->validator($request->all())->validate();  // Validate filled info
        
        if(!Auth::Check())  return \Redirect::back()->withErrors(['Je moet ingelogd zijn om een team aan te maken.']);
        $user = Auth::user();   // Logged in user
        $role = Role::Find($user->role_id); // Their role

        // If updating a team, get team from id. Else create a new team.
        if($update)
        {
            $team = Team::find($request->id);
            if(!$team)  return \Redirect::back()->withErrors(['Team niet gevonden.']);
        }
        else
            $team = new Team();

        // If user is team manager or higher, they are allowed to alter teams :)
        if($role->permission < 50 && $team->leader_id !== $user->id)    return \Redirect::back()->withErrors(['Je hebt niet de juiste permissies om een team aan te maken.']);

        // Set name
        $team->name = $request->name;

        // If leader ID is given in request, set leader id
        if($request->leader_id)
            $team->leader_id = $request->leader_id;
        elseif($request->leader)    // If no id is given, search the leader based on the given name instead
        {
            $user_controller = new UserController;
            $leader = $user_controller->search($request->leader);
            $leader_id = json_decode(json_encode($leader))->original[0]->id;
            if($leader) $team->leader_id = $leader_id;
            else        abort(404); //TODO: Proper erroring
        }
        else    return \Redirect::back()->withErrors(['Je hebt geen team leider ingesteld.']);

        // Save team
        $team->save();

        // Remove all players from the tea
        $team->players()->detach();
        // Add given players
        if($request->users)
        {
            foreach($request->users as $player)
            {
                if(!$this->isInTeam($player, $team->id))
                    $team->players()->attach($player);
            }
        }

        // if avatar is provided, upload it and save it to the user
        if($request->hasFile('logo'))
        {
            // upload image
            $image_controller = new ImageController;
            $image_path = $image_controller->uploadImage($request->file('logo'), 'teams', $team->id);
            // set to DB
            if($image_path)
            {
                $team->logo = $image_path;
                $team->save();
            }
        }

        // Redirect back
        return redirect(route('edit.team.route', $team->id));    // TODO: Message that team has been created4
    }
    

    /**
     * Edit a team
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {

        return $this->create($request, true);
    }
    

    /**
     * Delete team with specified id.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function remove(int $id)
    {
        if(!Auth::Check())  return \Redirect::back()->withErrors(['Je moet ingelogd zijn om een team te verwijderen.']);
        $user = Auth::User();

        $team = Team::find($id);
        if(!$team)   return \Redirect::back()->withErrors(['Team niet gevonden.']);

        // the leader of the team and organisers can remove teams.
        if($team->leader_id == $user->id || $user->role->permission >= 50)
        {
            $team->delete();
            return redirect(route('teams'));
        }
        else    return \Redirect::back()->withErrors(['Je hebt niet de juiste permissies om dit team te verwijderen.']);
    }
    

    /**
     * Delete a team from post request
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function removeByRequest(Request $request)
    {
        return $this->remove($request->id);
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,
        [
            'name' => 'required|string|max:199',
            'logo' => 'image|nullable',
            'leader' => 'required|string',
            'leader_id' => 'integer|nullable',
        ]);
    }


    /**
     * redirect to page that allows user to create a team
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function showNewForm()
    {
        if(!Auth::Check())  return \Redirect::back()->withErrors(['Je moet ingelogd zijn om een nieuw team aan te maken.']);
        $user = Auth::User();

        if($user->role->permission >= 30)
        {
            return view('/pages/new-team')->with('creating', true)->with('user', $user);
        }
        else    return \Redirect::back()->withErrors(['Je hebt niet de juiste permissies om een team aan te maken.']);

        return redirect(route('home'));
    }


    /**
     * redirect to page that allows user to edit team with given id
     *
     * @param   int $id
     * @return \Illuminate\Http\Response
     */
    public function showEditForm(int $id)
    {
        if(!Auth::Check())  return \Redirect::back()->withErrors(['Je moet ingelogd zijn om een team aan te passen.']);
        $user = Auth::User();

        $team = Team::find($id);
        if(!$team)  return \Redirect::back()->withErrors(['Team niet gevonden.']);

        // The team leader OR organisers are allowed to edit teams
        if($user->id == $team->leader_id || $user->role->permission >= 50)
        {
            $leader = $team->leader;

            return view('/pages/new-team')
                ->with('creating', false)
                ->with('team', $team)
                ->with('leader_name', $leader->getFullName())
                ->with('players', $team->players)
                ->with('user', $user);
        }
        else    return \Redirect::back()->withErrors(['Je hebt niet de juiste permissies om dit team aan te passen.']);

        return redirect(route('home'));
    }
}
