<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Image Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the uploading and removing of (public) images.
    |
    */
    
    /**
     *  Upload an image to specified folder.
     * 
     * @param   Illuminate\Http\UploadedFile    $image
     * @param   string|null    $folder
     * @param   string|null    $image_name
     * @return  string|false
     */
    public function uploadImage($image, $folder=null, $image_name=null)
    {
        if($image->isValid())
        {
            // If no image name is given, generate an unique one based on time.
            if($image_name == null) $image_name = md5(microtime());

            // Where the image will be stored
            $images_root = ($folder) ?
            '/images/' . $folder . '/' : 
            '/images/';
            // If folder is specified, check if it exists and if not create it.
            if($folder) $this->createFolder($folder);

            // If there's an already image, remove it.
            if(file_exists($images_root . $image_name))    removeImage($image_name, $folder);

            // Save image
            $image->move(public_path() . $images_root, $image_name . '.' . $image->getClientOriginalExtension());

            // Return full path to image
            return $images_root . $image_name . '.' . $image->getClientOriginalExtension();
        }
        else    return false;
    }


    /**
     *  Remove specified image. Returns true if removed. Returns false if image didnt exist.
     * 
     * @param   string    $image_name
     * @param   string|null    $folder
     * @return  bool
     */
    public function removeImage($image_name, $folder=null)
    {
        // Include folder in image path depending on if it's given
        $image = ($folder) ?
            public_path() . '\images\\' . $folder . '\\' . $image_name : 
            public_path() . '\images\\' . $image_name;
        // If folder is specified, check if it exists and if not create it.
        if($folder) $this->createFolder($folder);

        // If image exists, remove it and return true. Else return false.
        if(file_exists($image))
        {
            unlink($image);
            return true;
        }
        else    return false;
    }


    /**
     *  Checks if the folder exists. if it doesn't it'll create it.
     * 
     * @param   string  $folder
     * @return  void
     */
    protected function createFolder($folder)
    {
        clearstatcache();
        $folder_path = public_path() . '/images/' . $folder . '/';   // Get path
        if(!file_exists($folder_path))  mkdir($folder_path);    // Check if it exists, if it doesnt create the folder.
    }
}
