<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;

class LastSeen
{
    /**
     * Handle an incoming request. Updates an user's last seen.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // If logged in
        if (Auth::check())
        {
            $user = Auth::user();   // Get user
            $user->last_seen = new \DateTime(); // Update last seen to now
            $user->save();
        }

        return $next($request); // Continue
    }
}
