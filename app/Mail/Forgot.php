<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Forgot extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * link to reset password
     *
     * @var string
     */
    private $link;

    /**
     * The subject of the message.
     *
     * @var string
     */
    public $subject = "Wachtwoord vergeten?";

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($link)
    {
        $this->link = $link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails/reset')->with(['link' => $this->link]);
    }
}
