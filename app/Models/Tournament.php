<?php

namespace App;
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\DateController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Models\Pool;
use DateTime;

class Tournament extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    public $table = 'tournament';

    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;


    /**
     * The attributes that are mass assignable. 
     *
     * @var array
     */
    protected $fillable = [
        'name', 'start_date', 'end_date', 'mott_id'
    ];


    /**
     * Get dutch formatted date
     * 
     * @return array
     */
    public function getDutchDate($year=true)
    {
        $date_controller = new DateController;
        $dates = array(
            'start' =>  $date_controller->dutchDate($this->start_date, $year),
            'end'   =>  $date_controller->dutchDate($this->end_date, $year)
        );
        return $dates;
        // return "{$this->first_name} {$this->sur_name}";
    }


    /**
     * Get the pools that belongs to this tournament
     */
    public function pools()
    {
        return $this->hasMany('App\Models\Pool');
    }


    /**
     * Get the man of the tournament
     * 
     * @return \App\Models\User
     */
    public function mott()
    {
        return $this->belongsTo('App\Models\User', 'mott_id');
    }



    /**
     * Get the matches that belongs to this tournament
     */
    public function matches()
    {
        return $this->hasManyThrough('App\Models\Match', 'App\Models\Pool');
    }

    public function extraMatches()
    {
        return $this->hasMany('App\Models\Match');
    }


    public function myPools($get=null, $user_id=null)
    {
        if($user_id)
            $id = $user_id;
        else
        {
            $user = Auth::user();
            $id = $user->id;
        }
        
        $pools = $this->pools()->whereHas('teams.players', function($query) use($id)
        {
           $query->where('id', '=', $id);
        });
        // Return found tournaments. use GET command if given.
        return ($get) ? $pools->get() : $pools;

    }



    /**
     * Get the highest pool number
     */
    public function poolNumber()
    {
        $pools = $this->pools();
        $pool = ($pools) ? $pools->orderBy('number', 'desc')->first() : null;
        return ($pool) ? $pool->number : 0;
    }


    /**
     * Get all matches in this tournament an user belongs to
     * @param int $user_id
     * @param int|null $has_ended
     * @return App\Models\Match
     */
    public function myMatches($user_id, $has_ended=null)
    {
        // Check for matches registered through pools
        if($has_ended !== null)
            $matches = $this->matches()->where('has_ended', $has_ended);
        else
            $matches = $this->matches();

        $my_match = $matches->whereNotNull('start')->whereHas('result1.team.players', function($query) use($user_id)
        {
           $query->where('id', '=', $user_id);
        })
        ->orWhereHas('result2.team.players', function($query) use($user_id)
        {
            $query->where('id', '=', $user_id);
        });
        
        #
        // Return found tournaments. use GET command if given.
        return $my_match->orderBy('start');
    }


    /**
     * Get all matches in this tournament an user belongs to
     * @param int $user_id
     * @param int|null $has_ended
     * @return App\Models\Match
     */
    public function myExtraMatches($user_id, $has_ended=null)
    {
        $matches = $this->extraMatches();
        
        $matches = $matches->whereHas('result1.team.players', function($query) use($user_id)
        {
           $query->where('id', '=', $user_id);
        })
        ->orWhereHas('result2.team.players', function($query) use($user_id)
        {
            $query->where('id', '=', $user_id);
        });
        
        if($has_ended !== null)
            $matches = $matches->where('has_ended', $has_ended);

        // Return found tournaments. use GET command if given.
        return $matches->orderBy('start');
    }


    

    /**
     * Get a given player's first upcoming match within this tournament
     * 
     * @param int $user_id
     * @return App\Models\Match|false
     */
    public function myFirstMatch($user_id)
    {
        $matches1 = $this->myMatches($user_id, 0)->get();
        $matches2 = $this->myExtraMatches($user_id, 0)->get();
        $matches_combined = $matches1->merge($matches2);

        if($matches_combined)    // if the player has any matches in this tournament
        {
            $matches = array();
            foreach($matches_combined as $key => $match)
            {
                if( $match->has_ended == 0 )
                    $matches[] = $match;
            }

            // if there's more than 1 sort them
            if(sizeof($matches) > 1)
            {
                foreach ($matches as $key => $match)    // for each match
                    $sort[$key] = strtotime($match->start);   // Convert start time to a timestamp and store it in an array
                
                // $matches = $matches->all();
                array_multisort($sort, SORT_DESC, $matches); // Sort the matches based on the timestamps
                $matches = array_reverse($matches);  // Reverse it so that the earliest matches are first
                
                return $matches[0];
            }
            elseif(sizeof($matches) == 1)    return $matches[0];   // if there's only 1 match, return that
        }
        return false;
    }
}
