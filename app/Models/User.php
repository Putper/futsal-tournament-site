<?php

namespace App\Models;


use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\MailResetPasswordToken;
use Illuminate\Support\Facades\DB;
use App\Models\Tournament;


class User extends Authenticatable
{
    use Notifiable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    public $table = 'user';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable. 
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'sur_name', 'email', 'password', 'role_id', 'avatar', 'register_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'register_token', 
    ];


    /**
     * Get full name of the user
     * 
     * @return string
     */
    public function getFullName()
    {
        return "{$this->first_name} {$this->sur_name}";
    }


    /**
     * Get ID from random user, possible with specified id
     * 
     * @param integer $role
     * @return integer
     */
    public function getRandomUserId($role=null)
    {
        if($role)   return User::inRandomOrder()->whereNotNull('first_name')->where('role_id', $role)->first()->id;
        else    return User::inRandomOrder()->whereNotNull('first_name')->first()->id;
    }

    
    /**
     * Get user with given register token
     * 
     * @return App\Models\User
     */
    public function getUserWithToken($token)
    {
        return DB::table('user')->where('register_token', $token)->first();
    }

    
    /**
     * Get tournaments this user participates in.
     * 
     * @param boolean $get
     * @return App\Models\Tournament
     */
    public function tournaments($get=false)
    {
        $id = $this->id;
        $tournaments = Tournament::whereHas('pools.teams.players', function($query) use($id)
        {
           $query->where('id', '=', $id);
        });
        // Return found tournaments. use GET command if given.
        return ($get) ? $tournaments->get() : $tournaments;
    }


    /**
     * multi-column search with names. Permission can be given to limit finding users with that permission.
     * 
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  string  $name
     * @param  integer  $role_id
     * @return App\Models\User
     */
    public function scopeSearchName($query, $name, $role_id=null)
    {
        // Split each Name by Spaces
        $names = explode(" ", $name);

        return User::select('role_id','first_name', 'sur_name', 'id')
            ->where(function($query) use($names, $role_id)
        {
            // For each $names
            for ($i=0; $i<count($names); $i++)
            {
                // If role_id is given, limit to users that have a role with that role
                if($role_id)
                {
                    $query->where('role_id', '>=', $role_id);
                    // $query->whereHas('role', function($query) use($permission)
                    // {
                    //     $query->where('permission', $permission);
                    // });
                }
                $query->where('first_name', 'like', '%'.$names[$i].'%')
                    ->orWhere('sur_name', 'like', '%'.$names[$i].'%');
            }
        });
    }


    /**
     * Get the role that the user belongs to
     * 
     * @return \App\Models\Role
     */
    public function role()
    {
        return $this->belongsTo('App\Models\Role', 'role_id');
    }

    
    /**
     * Get the man of the tournaments the user has won.
     */
    public function motts()
    {
        return $this->hasMany('App\Models\Tournament');
    }

    
    /**
     * Get the man of the matches the user has won.
     */
    public function motms()
    {
        return $this->hasMany('App\Models\Result');
    }


    /**
     * Get the teams this user is leader of
     */
    public function leadingTeams()
    {
        return $this->hasMany('App\Models\User');
    }


    /**
     * The teams this user is in
     */
    public function teams()
    {
        return $this->belongsToMany('App\Models\Team');
    }


    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MailResetPasswordToken($token));
    }

    
    public function __toString()
    {
        return $this->getFullName();
    }
}
