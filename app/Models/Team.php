<?php

namespace App;
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Team extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    public $table = 'team';

    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;


    /**
     * The attributes that are mass assignable. 
     *
     * @var array
     */
    protected $fillable = [
        'name', 'logo', 'leader_id'
    ];


    /**
     * Get the leader of the team
     * 
     * @return \App\Models\User
     */
    public function leader()
    {
        return $this->belongsTo('App\Models\User', 'leader_id');
    }


    /**
     * The players in this team
     * 
     * @return \App\Models\User
     */
    public function players()
    {
        return $this->belongsToMany('App\Models\User')->withPivot('squad_number');
    }


    /**
     * The team belongs in pools
     */
    public function pools()
    {
        return $this->belongsToMany('App\Models\Pool');
    }


    /**
     * Get results that belong to this team
     * 
     * @return \App\Models\Result
     */
    public function results()
    {
        return $this->hasMany('App\Models\Result', 'team_id');
    }


    /**
     * Get the matches this team played in
     */
    public function matches()
    {
        // TODO: write function
    }


    /**
     * Get teams by $pool_id
     * @param int $pool_id
     * @return array $teams
     * @author Bjorn Zwikker <bjorzwikker@gmail.com>
     */
    public function getTeamsByPools($pool_id)
    {
        $raw_teams = DB::table('pool_team')->where('pool_id', $pool_id)->get();
        $teams = [];

        foreach ($raw_teams as $key => $team) 
        {
            $team = Team::find($team->team_id);
            array_push($teams, $team);
        }
        return $teams;
    }


    /**
     * multi-column search with names.
     * 
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  string  $name
     * @return App\Models\Team
     */
    public function scopeSearchName($query, $name)
    {
        return Team::select('name','logo', 'leader_id', 'id')
            ->where(function($query) use($name)
        {
            $query->where('name', 'like', '%'.$name.'%');
        });
    }

    /**
     * Add player to team_user collum
     * 
     * @param  int $user_id
     * @param  int  $team_id
     * @return bool
     */
    public function addPlayerToTeam($user_id, $team_id)
    {

        $teams_and_users = DB::table('team_user')->get()->all();

        foreach ($teams_and_users as $key => $team_and_user) 
        {
            if($team_and_user->user_id == $user_id && $team_and_user->team_id == $team_id)
            {
                continue;
            }
            elseif ($team_and_user->user_id != $user_id) 
            {
                continue;
            }
            else
            {
                //If a new player is found in the list then it will be inserted
                return DB::table('team_user')->insert(
                    ['team_id' => $team_id, 'user_id' => $user_id]
                );
            }
        }
    }
}
