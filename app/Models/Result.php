<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    public $table = 'result';

    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;


    /**
     * The attributes that are mass assignable. 
     *
     * @var array
     */
    protected $fillable = [
        'score', 'motm_id', 'team_id'
    ];



    /**
     * Get the team that plays in this match
     * 
     * @return \App\Models\Team
     */
    public function team()
    {   
        return $this->belongsTo('App\Models\Team', 'team_id');
    }


    /**
     * Get the man of the match
     * 
     * @return \App\Models\User
     */
    public function motm()
    {
        return $this->belongsTo('App\Models\Team', 'motm_id');
    }


    /**
     * The match the result belongs to
     * 
     * @return \App\Models\Match
     */
    public function result1()
    {
        return $this->belongsTo('App\Models\Match', 'result1_id');
    }


    /**
     * The match the result belongs to
     * 
     * @return \App\Models\Match
     */
    public function result2()
    {
        return $this->belongsTo('App\Models\Match', 'result2_id');
    }
}
