<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'email'; // or null

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    public $table = 'password_reset';


    /**
     * The attributes that are mass assignable. 
     *
     * @var array
     */
    protected $fillable = [
        'email', 'token', 'created_at'
    ];
}
