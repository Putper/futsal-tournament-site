<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pool extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    public $table = 'pool';

    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;


    /**
     * The attributes that are mass assignable. 
     *
     * @var array
     */
    protected $fillable = [
        'number', 'finished', 'tournament_id'
    ];


    /**
     * Get the tournament this pool belongs to
     * 
     * @return \App\Models\Tournament
     */
    public function tournament()
    {
        return $this->belongsTo('App\Models\Tournament', 'tournament_id');
    }

    
    /**
     * Get the matches that belongs to this tournament
     */
    public function matches()
    {
        return $this->hasMany('App\Models\Match');
    }


    /**
     * The teams in this pool
     * 
     * @return \App\Models\Team
     */
    public function teams()
    {
        return $this->belongsToMany('App\Models\Team');
    }
}
