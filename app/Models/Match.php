<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\DateController;

class Match extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    public $table = 'match';

    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;


    /**
     * The attributes that are mass assignable. 
     *
     * @var array
     */
    protected $fillable = [
        'number', 'finished', 'tournament_id'
    ];


    /**
     * Get the tournament this match belongs to
     * 
     * @return \App\Models\Tournament
     */
    public function tournament()
    {
        return $this->pool()->tournament();
    }


    public function extraTournament()
    {
        return $this->belongsTo('App\Models\Tournament', 'tournament_id');
    }


    /**
     * Get the tournament this match belongs to
     * 
     * @return \App\Models\Pool
     */
    public function pool()
    {
        return $this->hasOne('App\Models\Pool', 'pool_id');
    }

    
    /**
     * Get the first result.
     * 
     * @return \App\Models\Result
     */
    public function result1()
    {
        return $this->hasOne('App\Models\Result', 'id', 'result1_id');
    }


    /**
     * Get the second result.
     * 
     * @return \App\Models\Result
     */
    public function result2()
    {
        return $this->hasOne('App\Models\Result', 'id', 'result2_id');
    }


    /**
     * Get both results.
     * 
     * @return \App\Models\Result
     */
    public function results()
    {
        $results = array();
        $result[] = $this->result1();
        $result[] = $this->result2();
        return $results;
    }


    /**
     * Get dutch formatted date
     * 
     * @return array
     */
    public function getDutchDate($year=true)
    {
        $date_controller = new DateController;
        return $date_controller->dutchDate($this->start, $year);
    }



    // /** 
    //  * 
    //  * @param  \Illuminate\Database\Eloquent\Builder $query
    // */
    // public function scopeMyFirstMatch($query, $tournament_id, $user_id)
    // {
    //     $my_match = Match::where('tournament_id', $tournament_id)->get();
    //     // ->whereHas('result1.team.players', function($query) use($user_id)
    //     // {
    //     //    $query->where('id', '=', $user_id);
    //     // })
    //     // ->orWhereHas('result2.team.players', function($query) use($user_id)
    //     // {
    //     //     $query->where('id', '=', $user_id);
    //     // })->first();
    //     // $team1 = $my_match->result1()->team()->get();
    //     // $team2 = $my_match->result2()->team()->get();
        
    //     // Return found tournaments. use GET command if given.
    //     dd($my_match);
    // }
}
