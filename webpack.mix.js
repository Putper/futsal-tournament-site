let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    // main stuff, Gets loaded every page
    .babel(
        [
            'node_modules/materialize-css/dist/js/materialize.min.js',
            'resources/assets/js/app.js',
            'resources/assets/js/components/main-menu.js',
            // 'resources/assets/js/components/sponsors.js'
        ], 
        'public/js/app.js'
    )
   .sass('resources/assets/sass/app.scss', 'public/css')

   // Tournament pagen
   .babel(
       ['resources/assets/js/components/pool-classification.js'],
       'public/js/pages/tournament.js'
   )
   .sass('resources/assets/sass/pages/tournament.scss', 'public/css/pages')

   .babel(
       ['resources/assets/js/pages/cud-tournament.js'],
       'public/js/pages/cud-tournament.js'
    )
   
   // Teams Page
   .sass('resources/assets/sass/pages/teams.scss', 'public/css/pages')
   .babel(
       ['resources/assets/js/pages/new-team-form.js'],
       'public/js/pages/new-team.js'
    );
