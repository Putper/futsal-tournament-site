<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// Route::post('user/register', 'APIRegisterController@register');
// Route::post('user/login', 'APILoginController@login');

Route::post('user/register', 'Auth\Api\APIRegisterController@register');
Route::post('user/login', 'Auth\Api\APILoginController@login');
Route::get('teams', ['uses' => 'Auth\Api\APITeamController@index', 'middleware' => 'jwt.auth' ]);
Route::get('matchs',['uses' => 'Auth\Api\APIMatchController@index', 'middleware' => 'jwt.auth' ]);
Route::get('tournament', 'Auth\Api\APITournamentController@index' );
Route::get('poule', 'Auth\Api\APIPoolController@index');
Route::get('matches/{id}', 'Auth\Api\APITournamentController@viewTournament');
Route::get('eerstmatch/{id}', 'Auth\Api\APITournamentController@viewFirstmatch');
// Route::get('matches', 'Auth\Api\APIMatchController@index');
// Route::get('toernooien', 'pages\TournamentController@viewTournament')->name('tournament');

Route::middleware('jwt.auth')->get('users', function(Request $request) {
    $user = [JWTAuth::parseToken()->toUser()];

    return $user;
});




